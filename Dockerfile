# this is file to create Docker tomcat auto start 

FROM tomcat

MAINTAINER Pradip Tiwari

COPY tomcat-users.xml /usr/local/tomcat/conf/
COPY context.xml /usr/local/tomcat/webapps/manager/META-INF/

ENTRYPOINT ["/bin/sh", "-c", "/usr/local/tomcat/bin/startup.sh;bash"]
